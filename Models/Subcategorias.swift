//
//  File.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 14/08/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit

class Subcategorias: NSObject {
    var color: String!
    var descripcion: String!
    var img_background: String!
    var img_tumbnail: String!
    var titulo: String!
}
