//
//  Receta.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 16/08/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit

class Receta: NSObject {
    var img_backgroun: String!
    var img_tumbnail: String!
    var ingredients: String!
    var preparation: String!
    var recipe_book: String!
    var titleRecipe: String!
    var hexColor: String!
    
}
