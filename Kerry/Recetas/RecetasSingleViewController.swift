//
//  RecetasSingleViewController.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 16/08/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit
import Nuke

class RecetasSingleViewController: UIViewController, recetarioDelegate, SendRecipeDelegate {

    @IBOutlet weak var imageBackground: UIImageView!
    @IBOutlet weak var nameRecipe: UILabel!
    @IBOutlet weak var subnameRecipe: UILabel!
    @IBOutlet weak var ingredientsLabel: UILabel!
    @IBOutlet weak var ingredientsText: UILabel!
    @IBOutlet weak var prepLabel: UILabel!
    @IBOutlet weak var prepDesc: UILabel!
    
    @IBOutlet weak var preparacionTexto: UITextView!
    @IBOutlet weak var ingredientesTexto: UITextView!
    @IBOutlet weak var buttonSend: UIButton!
    @IBOutlet weak var viewColor: UIView!
    
    @IBOutlet weak var viewBackground: UIView!
    var SingleRecipe = Receta()
    var id = String()
    var color = String()
    var htmlBegin = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><style>body {font-family: BrandonGrotesque-Regular;font-size: 13px;color: #4A4A4A;} b {font-family: BrandonGrotesque-Regular;font-size: 13px; color:  #4A4A4A;}</style></head><body>"
    var htmlEnd = "</body></html>"
    var ws = RecetarioWS()
    override func viewDidLoad() {
        super.viewDidLoad()
        ws.delegate = self
        title = SingleRecipe.titleRecipe
//        viewBackground.backgroundColor = UIColor(hexString: color)
        let splitString = SingleRecipe.titleRecipe.split(separator: " ")
        if splitString.count > 1 {
            var string = String()
            for i in 0..<splitString.count - 1{
                string.append(splitString[i] + " ")
            }
            nameRecipe.text = string
            subnameRecipe.text = splitString.last?.description
        }else{
            nameRecipe.text = SingleRecipe.titleRecipe
            subnameRecipe.text = ""
        }
        
        if let ingredients = SingleRecipe.ingredients {
            let string = htmlBegin + ingredients + htmlEnd
            ingredientesTexto.attributedText = string.htmlToAttributedString
        }
        
        if let preparacion = SingleRecipe.preparation {
            let string = htmlBegin + preparacion + htmlEnd
            preparacionTexto.attributedText = string.htmlToAttributedString
        }
        
        if let imageBack = SingleRecipe.img_backgroun {
            Nuke.loadImage(with: URL(string: imageBack)!, into: imageBackground)
            print(imageBack)
            imageBackground.contentMode = .scaleAspectFill
            imageBackground.clipsToBounds = true
        }
        if let hexColor = SingleRecipe.hexColor {
            subnameRecipe.textColor = UIColor(hexString: hexColor)
            ingredientsLabel.textColor = UIColor(hexString: hexColor)
            prepLabel.textColor = UIColor(hexString: hexColor)
            viewColor.backgroundColor = UIColor(hexString: hexColor)
        }
        if color == "#000000" {
            nameRecipe.textColor = UIColor.white
            ingredientesTexto.textColor = UIColor.white
            preparacionTexto.textColor = UIColor.white
        }else{
            nameRecipe.textColor = UIColor(hexString: "#494949")
            ingredientesTexto.textColor = UIColor(hexString: "#494949")
            preparacionTexto.textColor = UIColor(hexString: "#494949")
        }
        // Do any additional setup after loading the view.
    }

    @IBAction func sendReciper(_ sender: Any) {
        let solicitarReceta = SolicitarRecetarioViewController(nibName: "SolicitarRecetarioViewController", bundle: nil)
        solicitarReceta.modalTransitionStyle = .crossDissolve
        solicitarReceta.modalPresentationStyle = .overCurrentContext
        solicitarReceta.delegate = self
        self.present(solicitarReceta, animated: true, completion: nil)
    }
    
    func didSucceedSendInfo() {
        print("succed")
        self.view.isUserInteractionEnabled = true
        let gracias = SuccessMessageViewController(nibName: "SuccessMessageViewController", bundle: nil)
        gracias.modalTransitionStyle = .crossDissolve
        gracias.modalPresentationStyle = .overCurrentContext
        self.present(gracias, animated: true, completion: nil)
    }
    func didSucceedSendContact() {
        print("succed")
        self.view.isUserInteractionEnabled = true
        let gracias = SuccessMessageViewController(nibName: "SuccessMessageViewController", bundle: nil)
        gracias.modalTransitionStyle = .crossDissolve
        gracias.modalPresentationStyle = .overCurrentContext
        self.present(gracias, animated: true, completion: nil)
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func sendRecipe(name: String, mail: String) {
        if isValidEmail(testStr: mail) {
            ws.sendInfo(name: name, mail: mail, pk: id)
            self.view.isUserInteractionEnabled = false
        }else{
            let alert = UIAlertController(title: "Ups", message: "El correo electrónico proporcionado no es correcto", preferredStyle: .alert)
            let aceptar = UIAlertAction(title: "Aceptar", style: .default, handler: nil)
            alert.addAction(aceptar)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    func didFailSendInfo(error: String) {
        self.view.isUserInteractionEnabled = true
        let alert = UIAlertController(title: "Ups", message: "Ha ocurrido un error, intentalo de nuevo", preferredStyle: .alert)
        let action = UIAlertAction(title: "Aceptar", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
//        let alert = UIAlertController(title: "Ups", message: error, preferredStyle: .alert)
//        self.present(alert, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func contacto(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "contactoView")
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    @IBAction func categorias(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "categoriesMain")
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    @IBAction func home(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func recetas(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            print(controller)
            if controller.isKind(of: RecipesViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
