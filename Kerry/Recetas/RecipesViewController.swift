//
//  RecipesViewController.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 16/08/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit
import Firebase
import Nuke

class RecipesViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var ref: DatabaseQuery!
    var ids = [String]()
    var sliders = [String]()
    var titles = [String]()
    var background = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Recetas"
        ref = Database.database().reference().child("categorias_recetas")
        ref.observe(.childAdded, with: {(snapshot) -> Void in
            self.ids.append(snapshot.key)
            self.sliders.append((snapshot.value as! NSDictionary).value(forKey: "img_slider") as! String)
            self.titles.append((snapshot.value as! NSDictionary).value(forKey: "title") as! String)
            self.background.append((snapshot.value as! NSDictionary).value(forKey: "img_background_sub") as! String)
            self.collectionView.reloadData()
        })
        
        // Do any additional setup after loading the view.
    }

    @IBAction func contacto(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "contactoView")
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    @IBAction func Categorias(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "categoriesMain")
        self.navigationController?.pushViewController(newVC, animated: true)
        
    }
    @IBAction func home(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension RecipesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ids.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "especialidad", for: indexPath) as! EspecialidadCollectionViewCell
            Nuke.loadImage(with: URL(string: sliders[indexPath.row])!, into: cell.imageBackground)
//            cell.titleLabel.text = titles.split(separator: " ")
            let splitString = titles[indexPath.row].split(separator: " ")
            if splitString.count > 1 {
                var string = String()
                for i in 0..<splitString.count - 1{
                    string.append(splitString[i] + " ")
                }
                cell.titleLabel.text = string
                cell.subtitleLabel.text = splitString.last?.description
                print(string)
                print(splitString.last?.description ?? "")
            }else{
                cell.titleLabel.text = titles[indexPath.row]
                cell.subtitleLabel.text = ""
            }
            cell.indexPath = indexPath
            cell.delegate = self
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mixology", for: indexPath) as! MixologyCollectionViewCell
            Nuke.loadImage(with: URL(string: sliders[indexPath.row])!, into: cell.imageBackground)
            cell.indexPath = indexPath
            cell.delegate = self
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "recetasGrid") as! RecetasGridViewController
        newVC.id_type = ids[indexPath.row]
        newVC.titleLabel = titles[indexPath.row]
        newVC.background = background[indexPath.row]
        if indexPath.row % 2 == 0 {
            newVC.color = "#ffffff"
        }else{
            newVC.color = "#000000"
        }
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if ids.count == 1 {
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        }else{
            return CGSize(width: collectionView.frame.width / 2, height: collectionView.frame.height)
        }
        
    }
    
}
extension RecipesViewController: especialidadDelegate, mixologyDelegate {
    func hazTap(index: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "recetasGrid") as! RecetasGridViewController
        newVC.titleLabel = titles[index.row]
        newVC.id_type = ids[index.row]
        newVC.background = background[index.row]
        self.navigationController?.pushViewController(newVC, animated: true)
    }
}
