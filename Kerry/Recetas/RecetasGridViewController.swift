//
//  RecetasGridViewController.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 16/08/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit
import Firebase
import Nuke

class RecetasGridViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var id_type = String()
    var ref = DatabaseQuery()
    var recipes = [Receta]()
    var titleLabel = String()
    var background = String()
    var color = String()
    var ids = [String]()
    @IBOutlet weak var imageBackground: UIImageView!
    @IBOutlet weak var imageBackground2: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = titleLabel
        ref = Database.database().reference().child("categorias_recetas").child(id_type).child("receta")
        ref.observe(.childAdded, with: {(snapshot) -> Void in
            let recipe = Receta()
            recipe.img_backgroun = (snapshot.value as! NSDictionary).value(forKey: "img_background") as! String
            recipe.img_tumbnail = (snapshot.value as! NSDictionary).value(forKey: "img_thumbnail") as! String
            recipe.ingredients = (snapshot.value as! NSDictionary).value(forKey: "ingredients") as! String
            recipe.preparation = (snapshot.value as! NSDictionary).value(forKey: "preparation") as! String
            recipe.recipe_book = (snapshot.value as! NSDictionary).value(forKey: "recipe_book") as! String
            recipe.titleRecipe = (snapshot.value as! NSDictionary).value(forKey: "title") as! String
            recipe.hexColor = (snapshot.value as! NSDictionary).value(forKey: "color") as! String
            self.recipes.append(recipe)
            print(snapshot.key)
            self.ids.append(snapshot.key)
            self.collectionView.reloadData()
            
        })
        Nuke.loadImage(with: URL(string: background)!, into: imageBackground)
        Nuke.loadImage(with: URL(string: background)!, into: imageBackground2)
        imageBackground.contentMode = .scaleAspectFill
        imageBackground.clipsToBounds = true
        // Do any additional setup after loading the view.
    }

    @IBAction func contacto(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "contactoView")
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    @IBAction func categorias(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "categoriesMain")
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    @IBAction func Recetas(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            print(controller)
            if controller.isKind(of: RecipesViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    @IBAction func home(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension RecetasGridViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recipes.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recipes_grid", for: indexPath) as! GridCollectionViewCell
        Nuke.loadImage(with: URL(string: recipes[indexPath.row].img_tumbnail)!, into: cell.img_tumb)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "recetasSingle") as! RecetasSingleViewController
        newVC.SingleRecipe = recipes[indexPath.row]
        newVC.id = ids[indexPath.row]
        print(color)
        newVC.color = color
        self.navigationController?.pushViewController(newVC, animated: true)
    }
}
