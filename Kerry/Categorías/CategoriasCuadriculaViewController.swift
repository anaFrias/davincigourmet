//
//  CategoriasCuadriculaViewController.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 30/07/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit
import Firebase
import Nuke

class CategoriasCuadriculaViewController: UIViewController {

    var id = String()
    var titleProduct = String()
    var ref: DatabaseQuery!
    var tumbnail = [String]()
    var subName = [String]()
    var subcat = [Subcategorias]()
  
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = titleProduct
        // Do any additional setup after loading the view.
        ref = Database.database().reference().child("categories").child(id).child("subcategories")
        
        ref.observe(.childAdded, with: {(snapshot) -> Void in
            var subcatSingle = Subcategorias()
            subcatSingle.color = (snapshot.value as! NSDictionary).value(forKey: "color") as! String
            subcatSingle.descripcion = (snapshot.value as! NSDictionary).value(forKey: "description") as! String
            subcatSingle.img_background = (snapshot.value as! NSDictionary).value(forKey: "img_background") as! String
            subcatSingle.img_tumbnail = (snapshot.value as! NSDictionary).value(forKey: "img_thumbnail") as! String
            subcatSingle.titulo = (snapshot.value as! NSDictionary).value(forKey: "title") as! String
            self.subcat.append(subcatSingle)
            self.tumbnail.append((snapshot.value as! NSDictionary).value(forKey: "img_thumbnail") as! String)
            self.subName.append((snapshot.value as! NSDictionary).value(forKey: "title") as! String)
            self.collectionView.reloadData()
            print(self.subcat)
        })
        
        
    }
    @IBAction func categorias(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            print(controller)
            if controller.isKind(of: CategoriasViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    @IBAction func home(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CategoriasCuadriculaViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if tumbnail.count < 2 {
            return tumbnail.count
        }else{
            return tumbnail.count + 1
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if tumbnail.count < 2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "escenciales", for: indexPath) as! CuadriculaCollectionViewCell
            Nuke.loadImage(with: URL(string: tumbnail[indexPath.row])!, into: cell.image)
            cell.titleLabel.text = subName[indexPath.row].uppercased()
            cell.titleLabel.textColor = UIColor(hexString: subcat[indexPath.row].color)
            cell.viewLabel.backgroundColor = UIColor(hexString: subcat[indexPath.row].color)
            return cell
        }else{
            if indexPath.row == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "escenciales", for: indexPath) as! CuadriculaCollectionViewCell
//                            Nuke.loadImage(with: URL(string: tumbnail[indexPath.row])!, into: cell.image)
                cell.image.image = nil
                cell.titleLabel.text = "VER TODO"
                cell.titleLabel.textColor = UIColor.white
                cell.viewLabel.backgroundColor = UIColor.white
                return cell
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "escenciales", for: indexPath) as! CuadriculaCollectionViewCell
                Nuke.loadImage(with: URL(string: tumbnail[indexPath.row - 1])!, into: cell.image)
                cell.titleLabel.text = subName[indexPath.row - 1].uppercased()
                cell.titleLabel.textColor = UIColor(hexString: subcat[indexPath.row - 1].color)
                cell.viewLabel.backgroundColor = UIColor(hexString: subcat[indexPath.row - 1].color)
                return cell
            }
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if tumbnail.count < 2 {
           let detailStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let dvc = detailStoryBoard.instantiateViewController(withIdentifier: "singleCategoty") as! SingleCategoryViewController
            dvc.allProducts = subcat[indexPath.row - 1]
            dvc.showsArrow = false
            self.navigationController?.pushViewController(dvc, animated: true)
        }else{
            if indexPath.row == 0 {
                let detailStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let dvc = detailStoryBoard.instantiateViewController(withIdentifier: "rootSingle") as! RootSingleViewController
                dvc.greetings = subcat
                dvc.titleProd = titleProduct
                dvc.index = indexPath.row
                self.navigationController?.pushViewController(dvc, animated: true)
                
            }else{
                let detailStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let dvc = detailStoryBoard.instantiateViewController(withIdentifier: "singleCategoty") as! SingleCategoryViewController
                dvc.allProducts = subcat[indexPath.row - 1]
                dvc.catName = titleProduct
                dvc.showsArrow = false
                self.navigationController?.pushViewController(dvc, animated: true)
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if (subcat.count + 1) < 3 {
            return CGSize(width: 374.2, height: 405.1)
        }else{
            return CGSize(width: 261.9, height: 283.6)
        }
    }
    
}
