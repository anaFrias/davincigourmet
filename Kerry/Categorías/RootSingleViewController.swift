//
//  RootSingleViewController.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 14/08/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit
import EMPageViewController

class RootSingleViewController: UIViewController, EMPageViewControllerDelegate, EMPageViewControllerDataSource {

    @IBOutlet weak var viewPageController: UIView!
    var pageViewController: EMPageViewController?
    var currentView : SingleCategoryViewController?
    var button: UIButton?
    var buttonItem: UIBarButtonItem?
    var greetings: [Subcategorias] = []
    var index = Int()
    var titleProd = String()
    var showsArrow = Bool()
    
    
    @IBOutlet weak var pageControllel: UIPageControl!
    @IBOutlet weak var foward: UIButton!
    @IBOutlet weak var reverse: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
//        scrollTo(das: index)
    }
    override func viewDidLayoutSubviews() {
        let pageViewController = EMPageViewController()
        pageViewController.scrollView.frame = UIScreen.main.bounds
        pageViewController.delegate = self
        pageViewController.dataSource = self
        pageViewController.scrollView.bounces = false

        let currentViewController = self.viewController(at: 0)!
        pageViewController.selectViewController(currentViewController, direction: .forward, animated: false, completion: nil)
//
        self.addChildViewController(pageViewController)
        self.viewPageController.insertSubview(pageViewController.view, at: 0)
        pageViewController.didMove(toParentViewController: self)
        self.pageViewController = pageViewController
        
    }
    func viewController(at index: Int) -> SingleCategoryViewController? {
        if (self.greetings.count == 0) || (index < 0) || (index >= self.greetings.count) {
            return nil
        }
        
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "singleCategoty") as! SingleCategoryViewController
        viewController.allProducts = self.greetings[index]
        viewController.titleProduct = titleProd
        viewController.isSon = true
        viewController.index = index
        viewController.total = self.greetings.count
        viewController.showsArrow = false
        return viewController
    }

    @IBAction func forward(_ sender: Any) {
        print("forward")
        self.pageViewController!.scrollForward(animated: true, completion: nil)
    }
    @IBAction func reverse(_ sender: Any) {
        print("reverse")
        self.pageViewController!.scrollReverse(animated: true, completion: nil)
    }
    func scrollTo(das : Int) {        
        for (index, viewControllerGreeting) in greetings.enumerated() {
            print(das, index)
            if (index != das) {
                let viewController = self.viewController(at: das)!
                let direction:EMPageViewControllerNavigationDirection = index > das ? .forward : .reverse
                    self.pageViewController!.selectViewController(viewController, direction: direction, animated: true, completion: nil)
                
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func index(of viewController: SingleCategoryViewController) -> Int? {
        if let greeting: Subcategorias = viewController.allProducts {
            let titleString = "\(self.greetings.index(of: greeting)! + 1) of \(self.greetings.count)"
            self.title = titleString
            self.pageControllel.numberOfPages = self.greetings.count
            self.pageControllel.currentPage = self.greetings.index(of: greeting)!
            return self.greetings.index(of: greeting)
        } else {
            return nil
        }
    }
    @objc func menuAction(){
        
    }
    func em_pageViewController(_ pageViewController: EMPageViewController, willStartScrollingFrom startingViewController: UIViewController, destinationViewController: UIViewController) {
        
    }
    func em_pageViewController(_ pageViewController: EMPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        if let viewControllerIndex = self.index(of: viewController as! SingleCategoryViewController) {
            let beforeViewController = self.viewController(at: viewControllerIndex - 1)
            return beforeViewController
        } else {
            return nil
        }
    }
    func em_pageViewController(_ pageViewController: EMPageViewController, isScrollingFrom startingViewController: UIViewController, destinationViewController: UIViewController, progress: CGFloat) {
        
    }
    func em_pageViewController(_ pageViewController: EMPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        if let viewControllerIndex = self.index(of: viewController as! SingleCategoryViewController) {
            let afterViewController = self.viewController(at: viewControllerIndex + 1)
            return afterViewController
        } else {
            return nil
        }
        
    }
    
    func em_pageViewController(_ pageViewController: EMPageViewController, didFinishScrollingFrom startViewController: UIViewController?, destinationViewController: UIViewController, transitionSuccessful: Bool) {
        let startViewController = startViewController as! SingleCategoryViewController?
        let destinationViewController = destinationViewController as! SingleCategoryViewController

        // If the transition is successful, the new selected view controller is the destination view controller.
        // If it wasn't successful, the selected view controller is the start view controller
        if transitionSuccessful {

            if (self.index(of: destinationViewController) == 0) {
                self.reverse.isEnabled = false
                self.reverse.alpha = 0
            } else {
                self.reverse.isEnabled = true
                self.reverse.alpha = 1
            }

            if (self.index(of: destinationViewController) == self.greetings.count - 1) {
                self.foward.isEnabled = false
                self.foward.alpha = 0
            } else {
                self.foward.isEnabled = true
                self.foward.alpha = 1
            }
        }

//        print("Finished scrolling from \(startViewController?.greeting) to \(destinationViewController.greeting). Transition successful? \(transitionSuccessful)")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
