//
//  SingleCategoryViewController.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 30/07/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit
import Nuke

class SingleCategoryViewController: UIViewController {

    var id = String()
    var titleProduct = String()
    var productsDesc = String()
    var imgProducts = String()
    var allProducts = Subcategorias()
    var catName = String()
    var color = String()
    var index: Int!
    var total: Int!
    var isSon = false
    var showsArrow = false
    var htmlBegin = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><style>body {font-family: BrandonGrotesque-Medium;font-size: 13px;color: #ffffff;} b {font-family: BrandonGrotesque-Medium;font-size: 13px; color:  #ffffff;}</style></head><body>"
    var htmlEnd = "</body></html>"
    @IBOutlet weak var transformView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var lineColor: UIView!
    @IBOutlet weak var imageBackground: UIImageView!
    @IBOutlet weak var gotaColor: UIImageView!
    @IBOutlet weak var infoFlavors: UILabel!
    @IBOutlet weak var nombreCat: UILabel!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    var stringSplit = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let descripcion = allProducts.descripcion {
            let string1 = descripcion.replacingOccurrences(of: "<ul>", with: "")
            let string2 = string1.replacingOccurrences(of: "<li>", with: "")
            let string3 = string2.replacingOccurrences(of: "</ul>", with: "")
            let string4 = string3.replacingOccurrences(of: "</li>", with: ",")
            print(string4)
            let stringSplite = string4.split(separator: ",")
            for i in 0..<stringSplite.count {
                stringSplit.append(String(stringSplite[i]))
            }
            print(stringSplit)
            let string = htmlBegin + descripcion + htmlEnd
            infoFlavors.attributedText = string.htmlToAttributedString
        }else{
            let string = htmlBegin + productsDesc + htmlEnd
            infoFlavors.attributedText = string.htmlToAttributedString
        }
        transformView.transform = CGAffineTransform(rotationAngle: -(CGFloat.pi / 2))
        
        if let titulo = allProducts.titulo {
            titleLabel.text = titulo.uppercased()
            nombreCat.text = titulo
        }else{
            titleLabel.text = titleProduct
            nombreCat.text = catName
        }
        imageBackground.contentMode = .scaleAspectFill
        if let image = allProducts.img_background {
            Nuke.loadImage(with: URL(string: image)!, into: imageBackground)
        }else{
            Nuke.loadImage(with: URL(string: imgProducts)!, into: imageBackground)
            
        }
        
        if let color = allProducts.color {
            titleLabel.textColor = UIColor(hexString: color)
            lineColor.backgroundColor = UIColor(hexString: color)
            gotaColor.image = gotaColor.image?.withRenderingMode(.alwaysTemplate)
            gotaColor.tintColor = UIColor(hexString: color)
        }else{
            titleLabel.textColor = UIColor(hexString: color)
            lineColor.backgroundColor = UIColor(hexString: color)
            gotaColor.image = gotaColor.image?.withRenderingMode(.alwaysTemplate)
            gotaColor.tintColor = UIColor(hexString: color)
        }
  
            nextButton.alpha = 0
        
        
        // Do any additional setup after loading the view.
    }


    @IBAction func nextView(_ sender: Any) {
        
    }
    
    @IBAction func categorias(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
//            print(controller)
            if controller.isKind(of: CategoriasViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    @IBAction func HOME(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SingleCategoryViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return stringSplit.count
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "flavor", for: indexPath) as! FlavorsCellCollectionViewCell
        let string = htmlBegin + "<ul><li>\(stringSplit[indexPath.row])</li></ul>" + htmlEnd
        cell.flavorTitle.attributedText = string.htmlToAttributedString
        return cell
    }
}
