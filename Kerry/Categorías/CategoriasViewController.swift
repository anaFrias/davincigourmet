//
//  CategoriasViewController.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 30/07/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit
import Firebase
import Nuke

class CategoriasViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var ref: DatabaseQuery!
    var ids = [String]()
    var imgs = [String]()
    var titles = [String]()
    var hasSubcategories = [Bool]()
    var descriptions = [String]()
    var img_background = [String]()
    var colors = [String]()
    var products = [Subcategorias]()
    override func viewDidLoad() {
        super.viewDidLoad()

        ref = Database.database().reference().child("categories").queryOrdered(byChild: "orden")
        
        ref.observe(.childAdded, with: {(snapshot) -> Void in
            let subcategoria = Subcategorias()
            self.ids.append(snapshot.key)
            self.imgs.append((snapshot.value as! NSDictionary).value(forKey: "img_slider") as! String)
            self.hasSubcategories.append((snapshot.value as! NSDictionary).value(forKey: "has_subcategories") as! Bool)
            self.titles.append((snapshot.value as! NSDictionary).value(forKey: "title") as! String)
            self.img_background.append((snapshot.value as! NSDictionary).value(forKey: "img_background") as! String)
           
            self.descriptions.append((snapshot.value as! NSDictionary).value(forKey: "description") as! String)
            self.colors.append((snapshot.value as! NSDictionary).value(forKey: "color") as! String)
            subcategoria.color = (snapshot.value as! NSDictionary).value(forKey: "color") as! String
            subcategoria.descripcion = (snapshot.value as! NSDictionary).value(forKey: "description") as! String
            subcategoria.img_background = (snapshot.value as! NSDictionary).value(forKey: "img_background") as! String
            subcategoria.img_tumbnail = (snapshot.value as! NSDictionary).value(forKey: "img_slider") as! String
            subcategoria.titulo = (snapshot.value as! NSDictionary).value(forKey: "title") as! String
            self.products.append(subcategoria)
            self.collectionView.reloadData()
        })
        
        
        
        // Do any additional setup after loading the view.
    }

    @IBAction func Home(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}
extension CategoriasViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ids.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "classic", for: indexPath) as! CategoriesCollectionViewCell
        Nuke.loadImage(with: URL(string: imgs[indexPath.row])!, into: cell.img_background)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.hasSubcategories[indexPath.row] {
            let storyboard  = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "categories") as! CategoriasCuadriculaViewController
            vc.id = ids[indexPath.row]
            vc.titleProduct = titles[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let detailStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let dvc = detailStoryBoard.instantiateViewController(withIdentifier: "singleCategoty") as! SingleCategoryViewController
            dvc.titleProduct = titles[indexPath.row].uppercased()
            dvc.catName = titles[indexPath.row]
            dvc.productsDesc = descriptions[indexPath.row]
            dvc.imgProducts = img_background[indexPath.row]
            dvc.color = colors[indexPath.row]
            dvc.allProducts = products[indexPath.row]
            self.navigationController?.pushViewController(dvc, animated: true)

        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 247.0, height: 834)
    }
}
