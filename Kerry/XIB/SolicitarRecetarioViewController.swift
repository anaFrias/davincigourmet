//
//  SolicitarRecetarioViewController.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 16/08/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit

protocol SendRecipeDelegate {
    func sendRecipe(name: String, mail: String)
}

class SolicitarRecetarioViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var correo: UITextField!
    @IBOutlet weak var nombre: UITextField!
    @IBOutlet weak var buttonSelected: UIButton!
    @IBOutlet weak var asteriscoNombre: UILabel!
    @IBOutlet weak var asteriscoEmail: UILabel!
    @IBOutlet weak var camposObligatorios: UILabel!
    
    @IBOutlet weak var noValido: UILabel!
    
//    VIEWS
    @IBOutlet weak var viewNombre: UIView!
    @IBOutlet weak var viewMail: UIView!
    @IBOutlet weak var viewTerminos: UIView!
    
    var delegate: SendRecipeDelegate!
    override func viewDidLoad() {
        super.viewDidLoad()
        correo.delegate = self
        nombre.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func dismis(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func termsAndConditions(_ sender: UIButton) {
        print(sender.isSelected)
        if sender.isSelected {
            sender.isSelected = false
            
        }else{
            sender.isSelected = true
            viewTerminos.layer.borderColor = UIColor.clear.cgColor
            viewTerminos.layer.borderWidth = 0
        }
    }
    
    @IBAction func sendInfo(_ sender: Any) {
        if nombre.text != "", correo.text != "" {
            if isValidEmail(testStr: correo.text!) {
                if buttonSelected.isSelected {
                    self.delegate.sendRecipe(name: nombre.text!, mail: correo.text!)
                    self.dismiss(animated: true, completion: nil)
                }else{
                    viewTerminos.layer.borderColor = UIColor.init(red: 231/255, green: 29/255, blue: 114/255, alpha: 1).cgColor
                    viewTerminos.layer.borderWidth = 1
                }
            }else{
                noValido.alpha = 1
            }
        }else{
            if correo.text == "" {
                viewMail.layer.borderColor = UIColor.init(red: 231/255, green: 29/255, blue: 114/255, alpha: 1).cgColor
                viewMail.layer.borderWidth = 1
                asteriscoEmail.alpha = 1
            }
            if nombre.text == "" {
                viewNombre.layer.borderColor = UIColor.init(red: 231/255, green: 29/255, blue: 114/255, alpha: 1).cgColor
                viewNombre.layer.borderWidth = 1
                asteriscoNombre.alpha = 1
            }
            camposObligatorios.alpha = 1
        }
        
    }
    func isValidEmail(testStr:String?) -> Bool {
        guard testStr != nil else { return false }
        
        // at least one uppercase,
        // at least one digit
        // at least one lowercase
        // 8 characters total
        let mailTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[@])(?=.*[.]).{8,}")
        return mailTest.evaluate(with: testStr)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == correo {
            if correo.text == "" {
                viewMail.layer.borderColor = UIColor.init(red: 231/255, green: 29/255, blue: 114/255, alpha: 1).cgColor
                viewMail.layer.borderWidth = 1
                asteriscoEmail.alpha = 1
                camposObligatorios.alpha = 1
            }
            
        }else {
            if nombre.text == "" {
                viewNombre.layer.borderColor = UIColor.init(red: 231/255, green: 29/255, blue: 114/255, alpha: 1).cgColor
                viewNombre.layer.borderWidth = 1
                asteriscoNombre.alpha = 1
                camposObligatorios.alpha = 1
            }
        }
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == correo {
            if correo.text == "" {
                viewMail.layer.borderColor = UIColor.clear.cgColor
                viewMail.layer.borderWidth = 0
                asteriscoEmail.alpha = 0
                camposObligatorios.alpha = 0
            }
            noValido.alpha = 0
        }else {
            if nombre.text == "" {
                viewNombre.layer.borderColor = UIColor.clear.cgColor
                viewNombre.layer.borderWidth = 0
                asteriscoNombre.alpha = 0
                camposObligatorios.alpha = 0
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
