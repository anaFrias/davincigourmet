//
//  CountryCitiesPickerViewController.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 27/08/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit
protocol sendInfoDelegate {
    func sendInfoPicker(info: String, input: String)
}
class CountryCitiesPickerViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var pickerData: UIPickerView!
    var info = [String]()
    var delegate: sendInfoDelegate!
    var indexPath = IndexPath()
    var inputs = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerData.delegate = self
        pickerData.dataSource = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return info.count
    }
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        delegate.sendInfoPicker(info: info[row], indexPath: indexPath)
//    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return info[row]
        
    }
    @IBAction func dismiss(_ sender: Any) {
        let infoSelected = info[pickerData.selectedRow(inComponent: 0)]
        self.delegate?.sendInfoPicker(info: infoSelected, input: inputs)
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
