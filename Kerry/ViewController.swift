//
//  ViewController.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 27/07/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit
import Firebase
import Nuke

class ViewController: UIViewController {

    @IBOutlet weak var blurMenu: UIImageView!
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var ImageBackground: UIImageView!
    @IBOutlet weak var descriptionTitle: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleLabel2: UILabel!
    
    var htmlBegin = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\"><style>body {font-family: BrandonGrotesque-Regular;font-size: 18px;color: #ffffff;} b {font-family: BrandonGrotesque-Regular;font-size: 18px; color:  #ffffff;}</style></head><body>"
    var htmlEnd = "</body></html>"
    var ref: DatabaseQuery!
    var data = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.init(red: 255/255, green: 146/255, blue: 0/255, alpha: 1)
        
        ref = Database.database().reference().child("about")
        ref.observe(.childAdded, with: {(snapshot) -> Void in
            if snapshot.key == "description" {
                let string = snapshot.value as! String
                self.data.append(self.htmlBegin + string + self.htmlEnd)
                self.descriptionTitle.attributedText = self.data[0].htmlToAttributedString
            }else if snapshot.key == "title"{
                let string = snapshot.value as! String
                let string_full = string.split(separator: " ")
                self.titleLabel.text = string_full[0].description.uppercased()
                if string_full.count > 1 {
                    self.titleLabel2.text = string_full[1].description.uppercased()
                }
                self.data.append(snapshot.value as! String)
            }
            if self.data.count > 2 {
                Nuke.loadImage(with: URL(string: self.data[1])!, into: self.ImageBackground)
            }
            
        })
//        blurEffectView.frame = self.view.bounds
//        self.menuView.addSubview(blurEffectView)
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func categoriesBtn(_ sender: Any) {

    }
    @IBAction func recipesBtn(_ sender: Any) {
        print("Recipes")
    }
    @IBAction func contactBtn(_ sender: Any) {
        print("Contacto")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

