//
//  Extensions.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 30/07/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}
//extension UIColor{
//    func HexToColor(hexString: String, alpha:CGFloat? = 1.0) -> UIColor {
//        // Convert hex string to an integer
//        let hexint = Int(self.intFromHexString(hexStr: hexString))
//        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
//        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
//        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
//        let alpha = alpha!
//        // Create color object, specifying alpha as well
//        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
//        return color
//    }
//
//    func intFromHexString(hexStr: String) -> UInt32 {
//        var hexInt: UInt32 = 0
//        // Create scanner
//        let scanner: Scanner = Scanner(string: hexStr)
//        // Tell scanner to skip the # character
//        scanner.charactersToBeSkipped = CharacterSet.init(charactersIn: "#")
////        scanner.charactersToBeSkipped = NSCharacterSet(charactersInString: "#")
//        // Scan hex value
////        scanner.scanHexInt(&hexInt)
//        scanner.scanHexInt32(&hexInt)
//        return hexInt
//    }
//}
