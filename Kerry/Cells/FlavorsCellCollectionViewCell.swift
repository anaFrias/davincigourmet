//
//  FlavorsCellCollectionViewCell.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 29/08/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit

class FlavorsCellCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var flavorTitle: UILabel!
}
