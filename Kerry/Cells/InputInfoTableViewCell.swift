//
//  InputInfoTableViewCell.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 08/08/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit

protocol infoDelegate {
    func sendInformationContact(name: String, mail: String, age: String, country: String, state: String, profile: String, activity: String, message: String)
    func showsDropDown(info: [String], input: String)
}

class InputInfoTableViewCell: UITableViewCell, UITextFieldDelegate, UITextViewDelegate {

    @IBOutlet weak var buttonTerms: UIButton!
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var viewActivity: UIView!
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var viewState: UIView!
    @IBOutlet weak var viewContry: UIView!
    @IBOutlet weak var viewAge: UIView!
    @IBOutlet weak var viewMail: UIView!
    @IBOutlet weak var viewName: UIView!
    
    @IBOutlet weak var Texto: UITextField!
    @IBOutlet weak var messageText: UITextView!
    @IBOutlet weak var activityText: UITextField!
    @IBOutlet weak var perfilText: UITextField!
    @IBOutlet weak var stateText: UITextField!
    @IBOutlet weak var countryText: UITextField!
    @IBOutlet weak var ageText: UITextField!
    @IBOutlet weak var mailTex: UITextField!
    
    @IBOutlet weak var obligName: UILabel!
    @IBOutlet weak var obligMail: UILabel!
    @IBOutlet weak var obligAge: UILabel!
    @IBOutlet weak var obligCountry: UILabel!
    @IBOutlet weak var obligState: UILabel!
    @IBOutlet weak var obligPerfil: UILabel!
    @IBOutlet weak var obligActividad: UILabel!
    @IBOutlet weak var obligMessage: UILabel!
    
    var info_country = [String]()
    var info_states = [String]()
    var info_profiles = [String]()
    var inf_activities = [String]()
    
    var showWrongState = false
    var showWrongProfile = false
    var showWrongActivity = false
    @IBOutlet weak var stateButton: UIButton!
    var delegate: infoDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        Texto.delegate = self
        mailTex.delegate = self
        ageText.delegate = self
        messageText.delegate = self
        
        if showWrongState {
            viewContry.layer.borderColor = UIColor.red.cgColor
            viewContry.layer.borderWidth = 0
            obligCountry.alpha = 0
        }
        
        if showWrongProfile {
            viewProfile.layer.borderColor = UIColor.clear.cgColor
            viewProfile.layer.borderWidth = 0
            obligPerfil.alpha = 0
        }
        
        if showWrongActivity {
            viewActivity.layer.borderColor = UIColor.clear.cgColor
            viewActivity.layer.borderWidth = 0
            obligActividad.alpha = 0
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == Texto {
            viewName.layer.borderColor = UIColor.clear.cgColor
            viewName.layer.borderWidth = 0
            obligName.alpha = 0
            
        }
        
        viewMessage.layer.borderColor = UIColor.clear.cgColor
        viewMessage.layer.borderWidth = 0
        obligMessage.alpha = 0
        
        if textField == ageText{
            viewAge.layer.borderColor = UIColor.red.cgColor
            viewAge.layer.borderWidth = 0
            obligAge.alpha = 0
        }
        
        if textField == mailTex{
            viewMail.layer.borderColor = UIColor.red.cgColor
            viewMail.layer.borderWidth = 0
            obligMail.alpha = 0
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        viewMessage.layer.borderColor = UIColor.red.cgColor
        viewMessage.layer.borderWidth = 0
        obligMessage.alpha = 0
        textView.text = ""
    }
    @IBAction func showsConutry(_ sender: Any) {
        self.delegate.showsDropDown(info: info_country, input: "pais")
        stateText.isEnabled = true
        viewContry.layer.borderColor = UIColor.red.cgColor
        viewContry.layer.borderWidth = 0
        obligCountry.alpha = 0
    }

    @IBAction func showsState(_ sender: Any) {
        if countryText.text == "México" || countryText.text == "" {
            stateText.isEnabled = false
            self.delegate.showsDropDown(info: info_states, input: "estado")
        }else{
            stateText.isEnabled = true
//            stateText.text = ""
        }
//        viewState.layer.borderColor = UIColor.red.cgColor
//        viewState.layer.borderWidth = 0
//        obligState.alpha = 0
        
    }
    @IBAction func showsProfile(_ sender: Any) {
        self.delegate.showsDropDown(info: info_profiles, input: "perfil")
        viewProfile.layer.borderColor = UIColor.red.cgColor
        viewProfile.layer.borderWidth = 0
        obligPerfil.alpha = 0
    }

    @IBAction func showsActivities(_ sender: Any) {
        self.delegate.showsDropDown(info: inf_activities, input: "actividad")
        viewActivity.layer.borderColor = UIColor.red.cgColor
        viewActivity.layer.borderWidth = 0
        obligActividad.alpha = 0
    }
    
    @IBAction func buttonT(_ sender: Any) {
        buttonTerms.layer.borderColor = UIColor.clear.cgColor
        buttonTerms.layer.borderWidth = 0
        if buttonTerms.isSelected {
            buttonTerms.isSelected = false
        }else{
            buttonTerms.isSelected = true
            
        }
        
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    @IBAction func sendInfo(_ sender: Any) {
        if !isEmptyString(Texto.text!) {
            if !isEmptyString(mailTex.text!) {
                if !isEmptyString(countryText.text!) {
                    if !isEmptyString(stateText.text!) {
                        if !isEmptyString(perfilText.text!) {
                            if !isEmptyString(activityText.text!) {
                                if !isEmptyString(messageText.text!), messageText.text != "MENSAJE" {
                                    if buttonTerms.isSelected {
                                        var perfil = String()
                                        var actividad = String()
                                        if perfilText.text == "Distribuidor" {
                                            perfil = "1"
                                        }else if perfilText.text == "Dueño de negocio" {
                                            perfil = "2"
                                        }else if perfilText.text == "Barista" {
                                            perfil = "3"
                                        }else if perfilText.text == "Chef" {
                                            perfil = "4"
                                        }else{
                                            perfil = "5"
                                        }
                                        
                                        if activityText.text == "Bebida de especialidad" {
                                            actividad = "1"
                                        }else if activityText.text == "Mixología" {
                                            actividad = "2"
                                        }else{
                                            actividad = "3"
                                        }
                                        if isValidEmail(testStr: mailTex.text!) {
                                            self.delegate.sendInformationContact(name: Texto.text!, mail: mailTex.text!, age: ageText.text!, country: countryText.text!, state: stateText.text!, profile: perfil, activity: actividad, message: messageText.text)
                                        }else{
                                            viewMail.layer.borderColor = UIColor.red.cgColor
                                            viewMail.layer.borderWidth = 1
                                            obligMail.alpha = 1
                                        }
                                    }else{
                                       showEmptyFields()
                                    }
                                    
                                }else{
                                  showEmptyFields()
                                }
                            }else{
                                showEmptyFields()
                            }
                        }else{
                           showEmptyFields()
                        }
                    }else{
                        showEmptyFields()
                    }
                }else{
                   showEmptyFields()
                }
            }else{
                showEmptyFields()
            }
        }else{
            showEmptyFields()
        }
            
//            if !isEmptyString(messageText.text) {
//
//            }else{
//                viewMessage.layer.borderColor = UIColor.red.cgColor
//                viewMessage.layer.borderWidth = 1
//                obligMessage.alpha = 1
//            }
//        }else{
//
//        }
        
    }
    func showEmptyFields() {
        if isEmptyString(Texto.text!) {
            // string contains non-whitespace characters
            viewName.layer.borderColor = UIColor.red.cgColor
            viewName.layer.borderWidth = 1
            obligName.alpha = 1
        }
        if isEmptyString(mailTex.text!) {
            viewMail.layer.borderColor = UIColor.red.cgColor
            viewMail.layer.borderWidth = 1
            obligMail.alpha = 1
        }
        if isEmptyString(ageText.text!)   {
            viewAge.layer.borderColor = UIColor.red.cgColor
            viewAge.layer.borderWidth = 1
            obligAge.alpha = 1
        }
        if isEmptyString(countryText.text!) {
            viewContry.layer.borderColor = UIColor.red.cgColor
            viewContry.layer.borderWidth = 1
            obligCountry.alpha = 1
        }
        if messageText.text == "MENSAJE" || isEmptyString(messageText.text) {
            viewMessage.layer.borderColor = UIColor.red.cgColor
            viewMessage.layer.borderWidth = 1
            obligMessage.alpha = 1
        }
        if !buttonTerms.isSelected{
            buttonTerms.layer.borderColor = UIColor.red.cgColor
            buttonTerms.layer.borderWidth = 1
            //                self.delegate.aceptaTerminos()
        }
        if isEmptyString(perfilText.text!)  {
            viewProfile.layer.borderColor = UIColor.red.cgColor
            viewProfile.layer.borderWidth = 1
            obligPerfil.alpha = 1
        }
        if isEmptyString(activityText.text!) {
            viewActivity.layer.borderColor = UIColor.red.cgColor
            viewActivity.layer.borderWidth = 1
            obligActividad.alpha = 1
        }
    }
    func isEmptyString(_ string: String) -> Bool {
        return string.trimmingCharacters(in: .whitespacesAndNewlines) == ""
        
    }
    
}
