//
//  SendButtonTableViewCell.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 08/08/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit

protocol sendInformation {
    func sndContacto()
}

class SendButtonTableViewCell: UITableViewCell {

    var terminos = Bool()
    var delegate:sendInformation!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func sendInfo(_ sender: Any) {
        self.delegate.sndContacto()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
