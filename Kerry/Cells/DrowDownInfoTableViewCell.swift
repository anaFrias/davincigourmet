//
//  DrowDownInfoTableViewCell.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 08/08/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit
protocol desplegarDelegate {
    func desplegarInfo(info: [String], index: IndexPath)
}

class DrowDownInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonDropDown: UIButton!
    @IBOutlet weak var Texto: UITextField!
    var delegate: desplegarDelegate!
    var info = [String]()
    var indexPath = IndexPath()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func desplegarInfo(_ sender: Any) {
        self.delegate.desplegarInfo(info: self.info, index: self.indexPath)
    }
    
}
