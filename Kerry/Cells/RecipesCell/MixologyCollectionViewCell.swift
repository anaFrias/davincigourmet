//
//  MixologyCollectionViewCell.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 16/08/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit
protocol mixologyDelegate {
    func hazTap(index: IndexPath)
}
class MixologyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageBackground: UIImageView!
    var delegate: mixologyDelegate!
    var indexPath = IndexPath()
    @IBOutlet weak var mixologyLabel: UILabel!
    @IBOutlet weak var colorView: UIView!
    @IBAction func verMas(_ sender: Any) {
        self.delegate.hazTap(index: indexPath)
    }
}
