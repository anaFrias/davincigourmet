//
//  EspecialidadCollectionViewCell.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 16/08/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit
protocol especialidadDelegate {
    func hazTap(index: IndexPath)
}
class EspecialidadCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageBackground: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var viewColor: UIView!
    var indexPath = IndexPath()
    var delegate: especialidadDelegate!
    @IBAction func verMas(_ sender: Any) {
        self.delegate.hazTap(index: indexPath)
    }
}
