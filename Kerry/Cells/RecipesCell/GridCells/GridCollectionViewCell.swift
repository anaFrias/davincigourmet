//
//  GridCollectionViewCell.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 16/08/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit

class GridCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img_tumb: UIImageView!
}
