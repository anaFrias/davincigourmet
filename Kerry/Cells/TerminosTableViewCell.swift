//
//  TerminosTableViewCell.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 08/08/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit

class TerminosTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonTerminos: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func AceptoTerminos(_ sender: Any) {
        if buttonTerminos.isSelected {
            buttonTerminos.isSelected = false
        }else{
            buttonTerminos.isSelected = true
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
