//
//  CuadriculaCollectionViewCell.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 13/08/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit

class CuadriculaCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var viewLabel: UIView!
    
}
