//
//  ContactoViewController.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 06/08/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit
import WebKit

class ContactoViewController: UIViewController/*, WKNavigationDelegate*/, UIScrollViewDelegate, desplegarDelegate, sendInfoDelegate, sendInformation, infoDelegate, recetarioDelegate {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var tableView: UITableView!
    var inputs = ["NOMBRE", "CORREO ELECTRÓNICO", "EDAD", "PAIS", "ESTADO", "PERFIL", "ACTIVIDAD", "MENSAJE", "TERMINOS", "ENVIAR"]
    var pais = String()
    var estado = String()
    var perfil = String()
    var actividad = String()
    var aceptaTerminos = Bool()
    var errorTerminos = Bool()
    var showDropDown = true
    var ws = RecetarioWS()
    var showWrongCountry = Bool()
    var showWrongProfile = Bool()
    var showWrongActivity = Bool()
    var empty = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ws.delegate = self
        
//        let url = URL(string: "http://go.kerry.com/l/293042/2017-08-29/9w8f2")
//        webView.load(URLRequest(url: url!))
//        webView.allowsBackForwardNavigationGestures = true
//        webView.sizeToFit()
//        print(webView.scrollView.zoomScale)
//        webView.scrollView.delegate = self
        // Do any additional setup after loading the view.
    }
    
//    override func loadView() {
//        webView.navigationDelegate = self
//
//    }
//    func scrollViewDidZoom(_ scrollView: UIScrollView) {
//        scrollView.setZoomScale(CGFloat(1.5), animated: true)
//    }
//    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
//
//    }
    func sendInfoPicker(info: String, input: String) {
        empty = false
        if input == "pais" {
            pais = info
            if info != "México" {
                showDropDown = false
            }else{
                showDropDown = true
            }
            estado = ""
            showWrongCountry = true
            tableView.reloadData()
        }else if input == "estado"{
            estado = info
            tableView.reloadData()
        }else if input == "perfil" {
            showWrongProfile = true
            perfil = info
            tableView.reloadData()
        }else{
            showWrongActivity = true
            actividad = info
            tableView.reloadData()
        }
        
    }
    
    func desplegarInfo(info: [String], index: IndexPath) {
        let openPicker = CountryCitiesPickerViewController(nibName: "CountryCitiesPickerViewController", bundle: nil)
        openPicker.modalPresentationStyle = .overCurrentContext
        openPicker.modalTransitionStyle = .crossDissolve
        openPicker.delegate = self
        openPicker.info = info
        openPicker.indexPath = index
//        openPicker.inputs = []
        self.present(openPicker, animated: true, completion: nil)
    }
    func sndContacto() {
        
    }
    func showsDropDown(info: [String], input: String) {
        let openPicker = CountryCitiesPickerViewController(nibName: "CountryCitiesPickerViewController", bundle: nil)
        openPicker.modalPresentationStyle = .overCurrentContext
        openPicker.modalTransitionStyle = .coverVertical
        openPicker.inputs = input
        openPicker.delegate = self
        openPicker.info = info
        self.present(openPicker, animated: true, completion: nil)
    }
    func sendInformationContact(name: String, mail: String, age: String, country: String, state: String, profile: String, activity: String, message: String) {
        self.view.isUserInteractionEnabled = false
        ws.sendContact(name: name, mail: mail, age: age, country: country, state: state, profile: profile, activity: activity, message: message)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func home(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func recetas(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "recipesStoryboard")
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    @IBAction func categorias(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let newVC = storyboard.instantiateViewController(withIdentifier: "categoriesMain")
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func didSucceedSendContact() {
        let gracias = SuccessMessageViewController(nibName: "SuccessMessageViewController", bundle: nil)
        gracias.modalTransitionStyle = .crossDissolve
        gracias.modalPresentationStyle = .overCurrentContext
        self.present(gracias, animated: true, completion: {
            self.view.isUserInteractionEnabled = true
            self.empty = true
            self.tableView.reloadData()
            
        })
    }

    func didFailSendContact(error: String) {
        let alert = UIAlertController(title: "Ups", message: "Ha ocurrido un error, intentalo de nuevo", preferredStyle: .alert)
        let action = UIAlertAction(title: "Aceptar", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
        self.view.isUserInteractionEnabled = true
        
    }
}
extension ContactoViewController: UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "name_info", for: indexPath) as! InputInfoTableViewCell
        
        cell.delegate = self
        cell.info_country = ["México", "Colombia", "Chile", "Guatemala", "Costa Rica", "Otro"]
        
        cell.info_states = ["Aguascalientes", "Baja California", "Baja California Sur", "Campeche", "Chiapas", "Chihuahua", "Coahuila de Zaragoza", "Colima", "Ciudad de México", "Durango", "Guanajuato", "Guerrero", "Hidalgo", "Jalisco", "Mexico", "Michoacan de Ocampo", "Morelos", "Nayarit", "Nuevo Leon", "Oaxaca", "Puebla", "Queretaro de Arteaga", "Quintana Roo", "San Luis Potosi", "Sinaloa", "Sonora", "Tabasco", "Tamaulipas", "Tlaxcala", "Veracruz","Yucatan", "Zacatecas"]
        
        cell.info_profiles = ["Distribuidor", "Dueño de negocio", "Barista", "Chef", "Otro"]
        
        cell.inf_activities = ["Bebida de especialidad", "Mixología", "Alimentos", "Otro"]
        if empty {
            cell.Texto.placeholder = "NOMBRE"
            cell.Texto.text = ""
            
            cell.mailTex.placeholder = "CORREO ELECTRÓNICO"
            cell.mailTex.text = ""
            
            
            cell.ageText.placeholder = "EDAD"
            cell.ageText.text = ""
            
            cell.messageText.text = "MENSAJE"
            
            cell.countryText.placeholder = "PAÍS"
            cell.countryText.text = ""
            
            cell.stateText.placeholder = "ESTADO"
            cell.stateText.text = ""
            
            cell.perfilText.placeholder = "PERFIL"
            cell.perfilText.text = ""
            
            cell.activityText.placeholder = "ACTIVIDAD"
            cell.activityText.text = ""
            
            cell.buttonTerms.isSelected = false
            pais = ""
            estado = ""
            perfil = ""
            actividad = ""
            cell.buttonTerms.isSelected = false
        }
        
        if showDropDown {
            cell.stateButton.alpha = 1
        }else{
            cell.stateButton.alpha = 0
        }
        
        if pais == "" {
            cell.countryText.placeholder = "PAÍS"
        }else{
            cell.countryText.text = pais
        }
        print(estado, "estado")
        if estado == "", cell.stateText.text == "" {
            cell.stateText.placeholder = "ESTADO"
            cell.stateText.text = ""
        }else{
            cell.stateText.text = estado
        }

        if perfil == "" {
            cell.perfilText.placeholder = "PERFIL"
        }else{
            cell.perfilText.text = perfil
        }

        if actividad == "" {
            cell.activityText.placeholder = "ACTIVIDAD"
        }else{
            cell.activityText.text = actividad
        }
        cell.showWrongActivity = showWrongActivity
        cell.showWrongState = showWrongCountry
        cell.showWrongProfile = showWrongProfile
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 472
    }
}
