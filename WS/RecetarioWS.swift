//
//  RecetarioWS.swift
//  Kerry
//
//  Created by Ana Victoria Frias on 16/08/18.
//  Copyright © 2018 InnovationWorkshop. All rights reserved.
//

import UIKit
import Alamofire

@objc protocol recetarioDelegate {
    @objc optional func didSucceedSendInfo()
    @objc optional func didFailSendInfo(error: String)
    
    @objc optional func didSucceedSendContact()
    @objc optional func didFailSendContact(error: String)
}

class RecetarioWS: NSObject {
    var delegate: recetarioDelegate!
    
    func sendInfo(name:String, mail:String, pk: String) {
        let parameters = [
            "name": name,
            "email": mail,
            "pk":pk
            ] as [String: String]
        Alamofire.request("http://expokerry.iwsandbox.com/api/send_email/", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate?.didSucceedSendContact!()
                        break
                    case 400:
                        self.delegate?.didFailSendInfo!(error: "No podemos procesar tu solicitud")
                        break
                    case 500:
                        self.delegate?.didFailSendInfo!(error: "Ha ocurrido un error, intenta de nuevo más tarde")
                        break
                    default:
                        self.delegate?.didFailSendInfo!(error: "Ha ocurrido un error, intenta de nuevo más tarde")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailSendInfo!(error: "Error de conexión, asegurate de estár conectado a internet")
                    break
                }
            })
    }
    
    func sendContact(name: String, mail: String, age: String, country: String, state: String, profile: String, activity: String, message: String) {
        let parameters = [
            "name": name,
            "email":mail,
            "years":age,
            "country": country,
            "state":state,
            "ideal":profile,
            "activity": activity,
            "message":message,
            "other_ideal":"",
            "other_activity": ""
            ] as [String: String]
        //        192.168.16.103:8000
//        expokerry.iwsandbox.com
        Alamofire.request("http://expokerry.iwsandbox.com/api/send_contact/", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseString(completionHandler: { (response) in
                print(response)
                switch response.result{
                case .success:
                    switch Int((response.response?.statusCode)!){
                    case 200:
                        self.delegate.didSucceedSendContact!()
                        break
                    case 400:
                        self.delegate?.didFailSendContact!(error: "No podemos procesar tu solicitud")
                        break
                    case 500:
                        self.delegate?.didFailSendContact!(error: "Ha ocurrido un error, intenta de nuevo más tarde")
                        break
                    default:
                        self.delegate?.didFailSendContact!(error: "Ha ocurrido un error, intenta de nuevo más tarde")
                        break
                    }
                    break
                case .failure( _):
                    self.delegate?.didFailSendContact!(error: "Error de conexión, asegurate de estár conectado a internet")
                    break
                }
            })
    }
}
